import lombok.Data;
import lombok.Setter;

import java.util.Objects;
@Data
public class Team implements Comparable<Team>{

    private String name;
    private Integer points;

    public Team(String name, Integer points) {
        this.name = name;
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public Integer getPoints() {
        return points;
    }


    @Override
    public String toString() {
        //Grouches, 0 pts
        String pointExtension = "pts";

        if(points == 1){
            pointExtension = "pt";
        }
        return name + ", " + points + " " + pointExtension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return name.equals(team.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    /**
     *Compares this object with the specified object for order. Returns a negative integer,
     * zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
     * First we compare by points. if the two teams are eaqual in points then we compare them by name
     * @param o the object to be compared.
     * @return Returns: negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
     */
    public int compareTo(Team o) throws NullPointerException{

        if(o == null){
            throw new NullPointerException("Cannot compare " + this + " to null object");
        }else {
            int compareResult = o.getPoints().compareTo(this.getPoints());

            if (compareResult == 0){
                compareResult = this.getName().compareToIgnoreCase(o.getName());
            }

            return compareResult;
        }
    }
}
