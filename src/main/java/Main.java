import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {


        boolean keepGoing = true;

        System.out.println("** Hello and welcome to the SPAN sports league table **\n");
        while(keepGoing){
            System.out.println("\n#@#@#@# MAIN MENU #@#@#@#");
            System.out.println("Enter the number of the option below to select how to enter league match results");
            System.out.println("(1) Enter the results using command line ");
            System.out.println("(2) Enter the results using text file");
            System.out.println("(3) Exit program");

            try {
                Scanner scanner = new Scanner(System.in);
                int selectionEnterResults = scanner.nextInt();
                scanner.skip("(\r\n|[\n\r])?");//nextInt leaves line terminators behind so skip them

                switch (selectionEnterResults){
                    case 1:
                        System.out.println("\nTaking input from Command line");
                        handleCommandLineLeagueTable(scanner);
                        break;
                    case 2:
                        System.out.println("\nTaking input from text file");
                        System.out.println("\nEnter the name of the input file containing match results: ");
                        String inputFileName = scanner.nextLine();
                        System.out.println("\nEnter the name of the output file to contain league table:");
                        String outputFileName = scanner.nextLine();
                        handleFileLeagueTable(inputFileName,outputFileName);
                        break;
                    case 3:
                        System.out.println("\nExit");
                        keepGoing = false;
                        scanner.close();
                        break;
                    default:
                        System.out.println("You have not selected a valid option, please try again\n");
                        break;
                }
            } catch (InputMismatchException e) {
                System.out.println("You have made an illegal selection, please try again\n");
            }catch (Exception e){
                System.out.println("There was an error processing an entry, please try again or exit program\n");
            }
        }

    }

    public static void handleCommandLineLeagueTable(Scanner scanner){
        boolean continueCmdLine = true;

        List<String> results = new LinkedList<>();

        while(continueCmdLine){

            System.out.println("Enter in a match sore and press enter, example: Lions 4, Tigers 3");

            String matchResult = scanner.nextLine();

            results.add(matchResult);

            System.out.println("\nDo you want to enter another match score");
            System.out.println("Enter (Y) for yes");
            System.out.println("Enter (N) for no");
            String selection = scanner.nextLine();

            if(selection.equalsIgnoreCase("N")){
                //calculate result table and print out

                System.out.println("\nBelow are the results of the league table\n");
                Map<String,Integer> leagueTable = generateLeagueTable(results);
                SortedSet<Team> sortedLeagueTable = sortLeagueTable(leagueTable);

                int i = 1;
                for(Team team: sortedLeagueTable){
                    System.out.println(i++ + ". " + team);
                }

                System.out.println("\n------------------------------------------------------\n");
                continueCmdLine = false;
            }
        }
    }

    /**
     * Handles the reading of match scores from an input file and writes the league table to the output file
     * @param inputFileName file name of the input file containing match scores
     * @param outputFileName file name of the output file for the league table
     */
    public static void handleFileLeagueTable(String inputFileName, String outputFileName) {

        File textFile = new File(inputFileName);

        Scanner fileScanner = null;
        BufferedWriter writer = null;

        try {
            fileScanner = new Scanner(textFile);

            List<String> fileResults = new LinkedList<String>();

            while (fileScanner.hasNextLine()) {
                fileResults.add(fileScanner.nextLine());
            }

            Map<String, Integer> leagueTable = generateLeagueTable(fileResults);
            SortedSet<Team> sortedLeagueTable = sortLeagueTable(leagueTable);

            writer = new BufferedWriter(new FileWriter(outputFileName));

            int i = 1;
            for (Team team : sortedLeagueTable) {
                writer.write(i++ + ". " + team.toString());
                writer.newLine();
            }

            System.out.println("\nFile write is complete!\n");

        } catch (FileNotFoundException e) {
            System.out.println("The file cannot be found, please try again or exit program\n");
        } catch (IOException e) {
            System.out.println("There was an error writing to the output file please try again or exit program\n");
        } finally {
            fileScanner.close();
            try {
                writer.close();
            } catch (IOException e) {
                System.out.println("There was an error writing to the output file please try again or exit program\n");
            }
        }
    }

    /**
     * Creates a map of the league based on match scores where the team name is the key and the points are the value
     * @param leagueResults list of strings representing match scotes
     * @return map of league
     */
    public static Map<String,Integer> generateLeagueTable(List<String> leagueResults){

        Map<String,Integer> leagueTable = new HashMap<String,Integer>();

        for(String result: leagueResults){

            String[] line = result.split(",");


            Integer scoreTeam1 = extractScore(line[0]);
            Integer scoreTeam2 = extractScore(line[1]);

            //replace all digits with empty space to leave only team name
            String team1Name = extractName(line[0]);
            String team2Name = extractName(line[1]);

            addTeamToLeagueTable(team1Name,leagueTable);
            addTeamToLeagueTable(team2Name,leagueTable);

            calculatePoints(team1Name,team2Name,scoreTeam1,scoreTeam2,leagueTable);

        }

        return leagueTable;
    }

    /**
     * replace all digits with empty space to leave only team name
     * @param str the name and the score
     * @return the name only without the score
     */
    public static String extractName(String str){
        String name =  str.replaceAll("\\d+", "").trim();
        if(name.isEmpty()){
            throw new RuntimeException("team name cannot be empty");
        }
        else {
            return name;
        }
    }

    /**
     * replace all non-digits with empty space to leave only the team score as Integer
     * @param str the name and the score
     * @return the score only as an Integer
     */
    public static Integer extractScore(String str){
        String scoreAsString = str.replaceAll("\\D+", "").trim();
        return Integer.parseInt(scoreAsString);
    }

    public static void addTeamToLeagueTable(String teamName,Map<String,Integer> leagueTable){

        if(!leagueTable.containsKey(teamName)){
            leagueTable.put(teamName,0);//team has 0 points when first entering the leagueTable
        }
    }

    /**
     * Created a set of sorted league table based on points then alphabetical order
     * @param leagueTable map of league
     * @return sorted set of league
     */
    public static SortedSet<Team> sortLeagueTable(Map<String,Integer> leagueTable){

        SortedSet<Team> sortedLeagueTable = new TreeSet<Team>();

        for (Map.Entry<String,Integer> entry : leagueTable.entrySet())
        {
            sortedLeagueTable.add(new Team(entry.getKey(),entry.getValue()));
        }

        return sortedLeagueTable;
    }

    /**
     * Calculates the points that each team should get and updates the league table
     * @param team1Name the name of team 1
     * @param team2Name the name of team 2
     * @param scoreTeam1 the score of team 1
     * @param scoreTeam2 the score of team 2
     * @param leagueTable the league table
     */
    public static void  calculatePoints(String team1Name,String team2Name, Integer scoreTeam1,Integer scoreTeam2,Map<String,Integer> leagueTable){

        //3 points for win, 1 point for tie, 0 for loose
        int winPoints = 3;
        int drawPoints = 1;

        int resultScore = scoreTeam1.compareTo(scoreTeam2);

        if(resultScore == 0){
            //there was a draw
            leagueTable.put(team1Name, leagueTable.get(team1Name) + drawPoints);
            leagueTable.put(team2Name, leagueTable.get(team2Name) + drawPoints);
        }else if(resultScore < 0){
            //team 2 won
            leagueTable.put(team2Name, leagueTable.get(team2Name) + winPoints);
        }else {
            //team 1 won
            leagueTable.put(team1Name, leagueTable.get(team1Name) + winPoints);
        }
    }
}
