import lombok.Cleanup;
import org.junit.Test;
import java.io.*;
import java.util.*;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class MainTest {

    @Test
    public void testCalculatePoints() {

        Map<String,Integer> leagueTable = new HashMap<String,Integer>();

        leagueTable.put("team-A",0);
        leagueTable.put("team-B",0);
        leagueTable.put("team-C",0);
        leagueTable.put("team-D",0);
        leagueTable.put("team-F",0);

        Main.calculatePoints("team-A","team-B",2,1,leagueTable);
        Main.calculatePoints("team-A","team-B",0,1,leagueTable);
        Main.calculatePoints("team-C","team-D",5,5,leagueTable);
        Main.calculatePoints("team-A","team-F",3,1,leagueTable);
        assertEquals(Integer.valueOf(6),leagueTable.get("team-A"));
        assertEquals(Integer.valueOf(3),leagueTable.get("team-B"));
        assertEquals(Integer.valueOf(1),leagueTable.get("team-C"));
        assertEquals(Integer.valueOf(1),leagueTable.get("team-D"));
        assertEquals(Integer.valueOf(0),leagueTable.get("team-F"));
    }

    @Test
    public void testAddTeamToLeagueTable(){
        Map<String,Integer> leagueTable = new HashMap<String,Integer>();

        Main.addTeamToLeagueTable("team-A",leagueTable);
        Main.addTeamToLeagueTable("team-B",leagueTable);
        Main.addTeamToLeagueTable("team-B",leagueTable);

        assertEquals(2,leagueTable.size());
        assertEquals(Integer.valueOf(0),leagueTable.get("team-A"));
        assertEquals(Integer.valueOf(0),leagueTable.get("team-B"));

    }

    @Test
    public void testSortLeagueTable(){
        Map<String,Integer> leagueTable = new HashMap<String,Integer>();

        leagueTable.put("team-F",1);
        leagueTable.put("team-Z",4);
        leagueTable.put("team-A",6);
        leagueTable.put("team-K",3);
        leagueTable.put("team-B",4);

        Set<Team> set = new TreeSet<Team>();

        set.add(new Team("team-A",6));
        set.add(new Team("team-B",4));
        set.add(new Team("team-Z",4));
        set.add(new Team("team-K",3));
        set.add(new Team("team-F",1));

        SortedSet<Team> sortedSet = Main.sortLeagueTable(leagueTable);



        assertEquals(true,sortedSet.equals(set));


    }

    @Test
    public void testExtractName(){
        String str = "FC Kabelo 55";
        String strName = Main.extractName(str);
        assertEquals("FC Kabelo",strName);
    }

    @Test
    public void testExtractScore(){
        String str = "FC Kabelo 55";
        Integer strScore = Main.extractScore(str);

        assertEquals(Integer.valueOf(55),strScore);
    }

    @Test
    public void testGenerateLeagueTable(){

        String[] s1  = {"Lions 3, Snakes 3"
                ,"Tarantulas 1, FC Awesome 0"
                ,"Lions 1, FC Awesome 1"
                ,"Tarantulas 3, Snakes 1"
                ,"Lions 4, Grouches 0"};

        List<String> leagueResults = Arrays.asList(s1);

        Map<String,Integer> leagueTable = new HashMap<String,Integer>();

        leagueTable.put("Lions",5);
        leagueTable.put("Snakes",1);
        leagueTable.put("Tarantulas",6);
        leagueTable.put("FC Awesome",1);
        leagueTable.put("Grouches",0);

        Map<String,Integer> generatedLeague = Main.generateLeagueTable(leagueResults);


        assertEquals(true,generatedLeague.equals(leagueTable));
    }

    @Test
    public void testHandleFileLeagueTable()  {

        File inputfile = new File("TestInput.txt");
        File file = null;

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(inputfile));

            int i = 1;
            String[] s1  = {"Lions 3, Snakes 3"
                    ,"Tarantulas 1, FC Awesome 0"
                    ,"Lions 1, FC Awesome 1"
                    ,"Tarantulas 3, Snakes 1"
                    ,"Lions 4, Grouches 0"};

            List<String> leagueResults = Arrays.asList(s1);

            for(String team: leagueResults){
                writer.write(team);
                writer.newLine();
            }

            writer.close();


            Main.handleFileLeagueTable("TestInput.txt","testOutput.txt");

            file = new File("testOutput.txt");

            assertEquals(true,file.exists());

            @Cleanup Scanner scanner  = new Scanner(file);

            assertEquals("1. Tarantulas, 6 pts",scanner.nextLine());
            assertEquals("2. Lions, 5 pts",scanner.nextLine());
            assertEquals("3. FC Awesome, 1 pt",scanner.nextLine());
            assertEquals("4. Snakes, 1 pt",scanner.nextLine());
            assertEquals("5. Grouches, 0 pts",scanner.nextLine());

        } catch (FileNotFoundException e) {
           fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }

        //clean up
        file.delete();
        inputfile.delete();


    }
}